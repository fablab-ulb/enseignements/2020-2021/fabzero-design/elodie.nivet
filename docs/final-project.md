# PROJET FINAL



![lampefinale](images/lampetissufinal.jpg)

MADEWITH est un luminaire d'ambiance constitué de tissus aux différents motifs en relief, créant des jeux de lumière et de texture. Ces tissus sont en réalité des vêtements déchets qui étaient voués à être jetés comme les autres tonnes jetés chaque année. 


Ce luminaire est une réinterprétation de la Rag lampe de Gaetano Pesce, enfin pas tout à fait... ce qui m'a inspiré ici est plutôt une philosophie engagée. L'italien est un artiste qui a révolutionné le monde du design en détournant la standardisation industrielle : à travers ces objets originaux, il critique certains points de la société. 
MADEWITH est une lampe confectionnée à partir de vêtements déchets et c'est par ce biais qu'elle devient un objet politique, en pointant du doigt l'industrie du textile, et ses tenants et aboutissants, à notre époque. 
Les différentes textures sont obtenues lorsqu'on chauffe le tissu sans qu'il soit brûlé. Selon la composition du matériau, celui-ci réagit de manière variée : rétractation, fonte, éffilement,... créant un panel de textures en relief.


## Conception

![croquisdim](images/dimensionslampetissu.jpg)



## Document SVG

![document svg](images/macrame.svg)



# RECHERCHES



![adam](images/LOGODAM.jpg)

Pour le premier jour de l'atelier, nous nous sommes donnés rendez-vous au [Design Museum Brussels](http://designmuseum.brussels/). Il nous a été demandé de déambuler durant 30 minutes dans l'espace muséal afin de choisir une pièce qui attirait notre attention, qui nous plaisait. Le but du jeu, après que chacun ait trouvé son objet, était de le réinterpréter à notre manière. 


## L'OBJET EN QUESTION  

![Photo de l'objet](images/Rag-lamp.jpg)

A Rag Table Lamp, par Gaetano Pesce, USA 1996. 
L’œuvre que j’ai choisi est une Lampe de table, qui fait partie de la collection Fish Design. « Rag » en anglais signifie « chiffon ». Effectivement, la lampe a bien l’aspect d’un bout de tissu avec ses drapés, pourtant elle est faite en **résine jaune et rouge**. Une ampoule est normalement posée sans support à l’intérieur. C’est une **pièce unique** qui fait partie d’une série de lampes qui reprennent exactement le même principe. 


### A PROPOS DE L'ARTISTE

![artist](images/geatano.jpg)

Né à La Spezia, en Italie, Gaetano est avant tout un architecte. Il étudie l’architecture de 1959 à 1965 à Venise, en même temps qu’il suit des cours à l’Institut du Design Industriel. 
C’est un **architecte humaniste** : il se bât contre les bâtiments froids, monolithiques, standardisés, et cherche plutôt à toucher les visiteurs avec une recherche sur l’optimisme, la surprise, la sensualité et la découverte. Son œuvre la plus connue est « Organic Building, à Osaka, construit en 1993.

En tant que designer, il refuse de se soumettre à la standardisation de l’industrie. Il fabrique pourtant des objets avec des matériaux artificiels comme la résine ou le polyuréthane, mais en dépassant les innovations technologiques pour les utiliser avec son propre langage. Il libère son imagination de tout diktat industriel et produit des objets du quotidien uniques, (pas de moules) où il joue sur l’imperfection. Pour lui, le design est un **terrain d’expérimentation** et non une simple réponse fonctionnelle à un problème donné.

![deuxième photo rag](images/rag2.jpg)


### POURQUOI CE CHOIX ? 

D'abord, cet objet a attisé ma curiosité car je trouvais que son aspect et son design, différaient complètement du reste de la collection du musée : tous les objets me semblaient très calculés, épurés, tandis que la lampe me paraîssait comme **aléatoire**. Par après j'ai appris qu'il s'agissait d'une oeuvre originale. 
Ensuite,j'ai toujours apprécié le textile pour ses drappés, et c'est cette idée de **mouvement figé** ou de légerté alourdie qui m'a plu. C'est un peu comme si on avait emprisonné un instant dans une forme. 
La matérialité de la lampe m'a également plu : elle est faite de résine, ce qui permet de jouer avec la **transparence**. J'ai juste trouvé dommage que la lampe ne soit pas éclairée. J'aurais voulu voir les jeux de lumière, la teinte et les projections que la lampe offre allumée. 


## DANS QUELLE DIRECTION JE VAIS

Cinq chemins de réinterprétation de notre objet nous ont eté proposés. Ces chemins vont définir la manière dont on va travailler sur un point, une idée, ou un concept qui nous intéresse dans l'objet de base. 

- REFERENCE
- INFLUENCE
- HOMMAGE
- INSPIRATION 
- EXTENSION 


### INSPIRATION 

Pour ma part, c'est le chemin qui me parle le plus. Voici quelques définitions selon des références différentes:
(J'ai rétiré quelques définitions qui ne servent pas mon propos, ex : la respiration)

#### Larousse

- Mouvement intérieur, impulsion qui porte à faire, à suggérer ou à conseiller quelque action : Suivre son inspiration.
- Enthousiasme, souffle créateur qui anime l'écrivain, l'artiste, le chercheur:Chercher l'inspiration.
- Ce qui est ainsi inspiré : De géniales inspirations.
- Influence exercée sur un auteur, sur une œuvre : Une décoration d'inspiration crétoise.

#### Le Petit Robert

- DIDACTIQUE Souffle émanant d'un être surnaturel, qui apporterait aux hommes des révélations. L'inspiration du Saint-Esprit.
- Souffle créateur qui anime les artistes, les chercheurs. L'inspiration poétique. Attendre l'inspiration.
- (ŒUVRE, ART) D'inspiration (+ ADJECTIF), qui s'inspire de (une œuvre du passé…). Une musique d'inspiration romantique.
- Idée, résolution spontanée, soudaine. Une heureuse inspiration.

#### wikipedia

- (Par analogie) Sorte de souffle divin qui pousse à tel ou tel acte.
Après la défaite de Koeniggraetz […], François-Joseph reçut du Saint-Esprit l’ordre d’appeler au pouvoir, […], un ministre saxon. L’inspiration céleste est ici manifeste, parce que le choix de l’Empereur ne saurait évidemment s’expliquer par aucune raison humaine. (Ernest Denis, La Question d’Autriche ; Les Slovaques, Paris, Delagrave, 1917, in-6, page
197)
- (Par extension) Acte de stimulation de l’intellect, des émotions et de la créativité à partir d’une influence.
La poésie du Divin et la poésie populaire, issues de classes sociales différentes, ont su parfois puiser aux mêmes sources d’inspiration : mais ce qui forme entre elles cloison étanche, c'est la forme de la langue : […]. — (Nimet Arzık, Anthologie des poètes turcs contemporains, Gallimard (NRF), 1953, page 10)
- (Art) Idée créatrice, élan créateur d'origine mystérieuse.
Cette faculté singulière, toujours dominante et jamais soumise, inégale, indisciplinable, impitoyable, venant à son heure et s’en allant comme elle était venue, ressemblait, à s’y méprendre, à ce que les poëtes nomment l’inspiration et personnifient dans la Muse. — (Eugène Fromentin, Dominique, L. Hachette et Cie, 1863, réédition Gründ, page 92)
Dans le secret de ton être peut se manifester l'inspiration. Ce mot, « inspiration », vient du latin in spiritu : cela signifie que tu es alors immergé dans la profondeur de ton propre esprit.
En cet instant privilégié s'entremêlent l'intuition, le sentiment, l'émotion et la joie. Tu as alors rendez-vous avec ta créativité. — (Jean Proulx, Grandir en humanité, Fides, 2018, page 54)

#### Littré

- Terme de physiologie. Action par laquelle l'air est inspiré, entre dans les poumons, mouvement opposé à l'expiration.Immédiatement après que l'air, chargé d'exhalaisons nuisibles, a été chassé au dehors par l'expiration, l'inspiration introduit dans le poumon un nouvel air, et avec lui bien des principes qui influent plus ou moins sur la sanguification,Bonnet,Contempl. nat. Œuvr. t. VIII, p. 30, note 1, dans POUGENS.Il paraît certain que les loups et les chiens ne hurlent que par inspiration ; on peut s'en assurer aisément, en faisant hurler un petit chien près du visage, on verra qu'il tire l'air dans sa poitrine, au lieu de le pousser au dehors,Buffon,Suppl à l'hist. nat. Œuvr. t. XI, p. 198, dans POUGENS.
- Fig.Mouvements de l'âme, pensées, actions qui sont dues à une insufflation divine comparée à l'insufflation qui introduit l'air dans la poitrine.Un soldat, poussé, dit Josèphe, par une inspiration divine, se fait lever par ses compagnons à une fenêtre et met le feu dans ce temple auguste [de Jérusalem],Bossuet,Hist. II, 8.Il [le jeune Scipion] attaque Carthage la Neuve, comme s'il eût agi par inspiration, et ses soldats l'emportent d'abord,Bossuet,Hist. I, 8.Qu'y a-t-il donc, chrétiens, qui puisse nous empêcher de recevoir, sans différer, ses
inspirations [de Dieu, pour la pénitence] ?Bossuet,Duch. d'Orl. On fait un pape par voie d'inspiration, lorsque tous les cardinaux s'accordent unanimement et comme par inspiration à choisir un sujet qu'ils nomment à haute voix.
- Particulièrement.L'enthousiasme qui entraîne les poëtes, les musiciens, les peintres. Ce vers a été écrit d'inspiration. Cette poésie est pleine d'inspiration.Vous ferez votre tragédie quand votre enthousiasme vous commandera ; car vous savez qu'il faut recevoir l'inspiration, et ne la jamais chercher,Voltaire,Lett. Chabanon, 20 août 1766.Qu'est-ce donc que l'inspiration ? l'art de lever un pan de voile, et de montrer aux hommes un coin ignoré ou plutôt oublié du monde qu'ils habitent, Diderot, Salon de 1767, Œuv. t. XIV, p. 368, dans POUGENS.Une inspiration céleste animant dans cet instant la physionomie de Corinne,Staël,Corinne, X, 5.
- Action de conseiller quelqu'un, de lui suggérer quelque chose.Marie [d'Angleterre], ne se dirigeant que par les inspirations de Philippe II, faisait brûler les protestants,Voltaire,Mœurs, 163. La chose inspirée. Je vous dois cette inspiration. Il n'écoute que les inspirations de sa fureur.

#### Ce que j'en retiens

L'inspiration est un enthousiasme, un souffle créateur qui anime un artiste. Elle prend sa source dans une influence, qui génère une stimulation de l'intellect, des émotions, de la créativité. Par une méthaphore de la respiration, on peut également comprendre ceci: à l'inspiration, l'air qui entre est composé de différentes particules, bonnes ou mauvaises pour nous. Lors de l'expiration, nous rejetons ce qui est nuisible pour notre corps. C'est à peu près la même chose avec l'inspiration: il s'agit d'un air nouveau, une idée nouvelle qui nous influencera plus ou moins dans notre démarche. 

### POURQUOI CE CHOIX 

Le choix du mot Inspiration m'est venu assez naturellement: précédemment j'avais expliqué les convictions de l'artiste, que je trouvais déjà inspirantes, et cela m'a donné envie de partir dans l'expérimentation. Gaetano Pesce ne voit pas le design et l'architecture comme quelque chose de figé: il lutte justement contre la standardisation industrielle et essaie de transmettre à travers ses oeuvres des sentiments nouveaux aux gens tels que l'étonnement, la surprise, la découverte. 
La question de l'imperfection m'attire aussi beaucoup : l'industrialisation donne accès à des produits relativement beaux et  abordables pour toutes les classes sociales, mais d'un autre coté, il y a quelque chose de froid qui se dégage de ses objets ou meubles. J'aurais tendance à avoir une préférence pour quelque chose qui a été fait artisanalement et je trouve même parfois que ce sont les défauts d'un objet qui lui donnent son charme. Quand je travaille, crée, expérimente, c'est bien souvent les "ratés" qui m'inspirent et me plaisent le plus. 

Ici, ce qui m'inspire n'est donc pas tant l'objet pour lui même, mais plutôt la manière dont l'artiste traite la philosophie cachée derrière l'objet.


## COMPRENDRE UN PEU MIEUX L'ARTISTE 

Gaetano Pesce est un **pionnier** du design italien et mondial de l'après-guerre. En plus du design, il pratique un bon nombre d'autres disciplines : architecture, peinture, sculpture, stylisme, scénographie, enseignement ainsi que philosophie contemporaine. Son travail est caractérisé par son inventive utilisation de couleurs et matériaux ainsi que les liens qu'il exprime sur la société et l'individu à travers l'art, l'architecture ou le design. 

Gaetano vivra de près la **contre-culture** : dans les années 60, une série de mouvements a lieu en Amérique dans le but de réagir aux dérives de l’American way of life, en plus de pointer du doigt la guerre du Vietnam, la surconsommation, l’emprise de l’Église et l’indifférence du gouvernement vis-à-vis de la destruction de l’écosystème. Vivant avec ce bagage, le multi-artiste a une forte personnalité et une vision ouverte sur le monde qui se traduit dans ses **oeuvres militantes et sociales**. Il **bouscule l'histoire du Design** en renversant la manière de le concevoir : selon lui, le design ne doit pas seulement être fonctionnel mais doit devenir un **objet de questionnement**. Autrement dit, il faut se confronter au monde et se servir du design pour le commenter. 

En 1969, il réalise une oeuvre emblématique :  le _Fauteuil UP5 Donna_. 

![fauteil](images/up5donna.jpg)

Paré de formes généreuses, voluptueuses, et avec une assise profonde et confortable, rattachée à un pouf rond par une chaine, le fauteil est représentatif de la situation de la femme qui est emprisonnée par la domination masculine. Selon les propres mots de l'artiste : « Cette réalisation m’a permis d’exprimer ma vision de la femme. Toujours sédentaire, elle reste malgré elle, prisonnière d’elle-même. La forme de ce fauteuil, évoquant les formes généreuses d’une femme, retenue par un boulet au pied, m’a permis de renvoyer à l’image traditionnelle du prisonnier ». On peut également y voir une autre possibilité, la mère et son enfant, critiquant la place qu'on donne à la mère et l'image qu'on se fait d'elle: l'image de la "super-maman". 

En 1972, il réalise la série de chaises Golgotha. 

![vhaises](images/golgotha.jpg)

Gaetano traite ici d'un autre sujet polémique : la religion. Les chaises sont réalisées à partir de fibres de verre et dacron d'abord imbibées de résine polyester pour être ensuite posées sur une forme. En séchant, la résine durcie donne alors forme à la fibre. Dans cette oeuvre, l'artiste fait directement référence au Suaire de Turin : c'est un drap de lin jaunâtre avec dessus une représentation floue du visage d'un homme portant les marques de blessures d'une crucifixion, faisant donc référence à la crucifixion de Jésus. Cela fait l'objet d'une piété populaire et est l'icône de l'Eglise Catholique. 
Avec cette oeuvre, Gaetano s'insurge également contre la standardisation des objets. En proposant cette méthode de "moulage", il détourne les objets industriels produits à l'identique.


## NOUVELLE DIRECTION 

Pour ma part, après l'analyse plus approfondie de ce que fait Gaetano, c'est plutôt vers une **critique de la société** que je veux me tourner pour conceptualiser mon objet. En faire un objet engagé qui a un sens politique.


## LES DECHETS DE LA SOCIETE

Un sujet qui me touche particulièrement est l'environnement. A l'heure actuelle, j'ai une vision très pessimiste de notre société, de la politique et du capitalisme. Si je peux résumer, dans vingt ou trente ans nous allons (l'Humain) nous éteindre. 

Voici ici un travail que j'avais du faire dans le cadre de mon projet : il nous était demandé de nous projeter dans 3 ans (quand nous sortirons de nos études d'architecture) ainsi que dans 25 ans. Pourquoi montrer ce travail ? Pour que vous compreniez un peu mieux dans quelle optique je me situe. 

> PORTRAIT D’ARCHITECTE 

> Tout d’abord parlons du contexte actuel. Le COVID pose pas mal de questions auxquelles il est difficile de répondre : Est-ce que cette politique de la peur va fonctionner sur les gens ? Vont-ils continuer à écouter et exécuter ou une rébellion est-elle envisageable ? Cette pandémie va-t-elle éveiller les consciences ? Comment allons-nous réagir face à la crise économique qui nous attend ? En clair, où va-t-on en arriver avec le corona virus. A mon sens, cette crise révèle beaucoup de problèmes inscrits dans notre système politique et sociétal actuel, mais aussi environnementalement. Je ne vais pas m’étaler plus sur le sujet mais vous aurez compris l’idée : est-ce que cette crise ne serait pas un tremplin pour un changement radical ou va-t-elle justement renforcer le gouffre déjà présent? 

>+3 ANS

>Juin 2023, je suis au travail, je vis le « rêve » que mon éducation reçue m’a apprise à envier. Le hic, c’est que le rêve qu’on m’offre à l’heure actuelle ne me convient pas. Je n’arrive pas à me visualiser, m’imaginer travailler dans ce système que je ne soutiens pas, sous simple prétexte que les choses sont faites comme cela et que l'on ne peut rien y changer. Je serais capable de continuer dans cette voie, mais je n’arriverais pas à m’épanouir de la sorte. J’ai encore beaucoup d’incertitudes sur la tournure que va prendre ma vie, d’ailleurs la question du « qui suis-je réellement » est encore assez floue, mais j’ai décidé de prendre mon destin en main. J’ai donc choisi d’entreprendre le voyage qui m’a toujours appelé, traverser les continents pour découvrir et apprendre sur tout ce dont la planète regorge (en végétation, savoirs, cultures, différences, merveilles, plaisirs...)  Je ne sais pas où tout cela va me mener, mais je suis sûre que cette aventure va m’aider à y voir plus clair et répondra à mes questions. 

>+25 ANS

>Plusieurs scénarios s’offrent à moi, alors prenons les deux extrêmes. Mais une chose me paraît importante à souligner : l’état d’avancement de la crise écologique qui est telle que: nous n’avançons pas dans un mur si on ne bouge pas maintenant, nous sommes déjà bien éclatés contre ce mur. Les extinctions massives des espèces, de la biomasse, les pertes de 60% des zones forestières,les changements climatiques et fontes des glaces nous le prouvent déjà. 

>Il y a un peu plus de 25 ans, la crise du corona virus a permis à la population de se conscientiser et de prendre les problèmes au sérieux … enfin pas exactement, ce n’était pas aussi facile que ça : une grosse majorité de personnes a compris que le gouvernement ne changerait rien à la situation car on se retrouvait confronté à un problème systémique. Le seul moyen de faire bouger les choses était donc d’agir, d’arrêter d’attendre désespérément une solution de la part de nos politiciens capitalistes. Rassemblements, mouvements, violences, guerres civiles, etc. Il ne fallait pas croire que les riches allaient se laisser faire. Du coup, d’autres tactiques ont été mises en place, un peu plus pacifistes. On voyait éclore de plus en plus de petites communautés qui militaient pour leurs droits et ceux de l’environnement, comme les ZAD le faisaient déjà à l’époque. Je faisais moi aussi partie de l'une d'entre elles. Doucement, nous avons suscité l’intérêt des autres et les mentalités se sont éveillées, de plus en plus de gens se sont joints à notre cause. Aujourd’hui, le monde a bien changé et même si nous avons réussi à ralentir les catastrophes environnementales, le combat continue car notre manière de vivre, plus traditionnelle, n’est toujours pas unanime. 

>Juin 2045, plus rien ne va: les températures atteignent des sommets mortels, la famine a fini par toucher les différents continents, l’eau potable se fait rare, les réserves des ressources planétaires sont épuisées, les guerres éclatent un peu partout, … Moi, je me réveille dans ma cabane conçue par mes propres mains avec l’aide de mes compagnons d’aventure. Il y a plus de 20 ans, j’ai fait un voyage qui a confirmé une envie que j’avais toujours eu au fond de moi, mais que je n’avais jamais osé entreprendre. J’ai décidé de partir vivre avec les personnes qui partageaient les mêmes convictions que moi et de créer ensemble notre propre communauté. Ici, je vis la vie dont j’ai toujours revé, loin des tensions politiques, des obligations de survie, des réprimandations, des incompréhensions, ... Nous ressentons certes, comme tous les terriens, les fluctuations climatiques et vivons avec une épée de Damocles au-dessus de la tête, mais au moins, nous avons la chance de vivre de la manière que nous avons choisi, où chaque être est pris en considération, est écouté, est libre de s’exprimer peu importe qu’il soit homme, femme, enfant, âgé, de telle religion, de telle couleur, … Notre système est horizontal et c’est ensemble que nous l’avons imaginé. 


### PREMIERE IDEE

Mon idée première est de travailler avec des déchets. Encore une fois, dans le cadre de mon projet, la thématique de cette année est le **réemploi de matériaux**. C'est un sujet qui me touche particulièrement et que j'ai aussi envie de traiter ici. Une question que je me suis alors posée est : quel type de déchet ai-je envie d'utiliser ? En premier lieu, j'ai pensé au plastique. 

#### Le plastique

Le plastique envahit notre quotidien. On le retrouve partout (dans les objets, textiles, revêtements de sol, meubles, isolants) mais notamment en grande quantité, et c'est ce qui pose le plus problème, du plastique à **usage unique jettable**. Bouteilles, emballages, produits hygiéniques, médicaments,etc. Par exemple, je m'insurge toujours de voir des fruits dits "bio" emballés dans un packaging plastique... What's the point ? Donc oui, le plastique n'est pas écologique. Seulement si son utilisation était réservée à un certain type de produits, ça ne serait pas tant un problème.

![etude](images/plasticdechets.jpg)

Un étude de [PlasticsEurope](https://www.plasticseurope.org/en/resources/publications/3-plastics-facts-2016) montre que 40% de la production de plastique est investie dans le secteur des emballages à usage unique. Or ces emballages ont donc une **durée de vie courte**.

Depuis 2 ans, des chercheurs essayaient de déterminer la quantité de déchets plastique que l'Homme a produit durant les dernières décénies. Depuis 1950, la production de plastique a accéléré à une vitesse incroyable et à l'heure actuelle, les chercheurs ont annoncé un point de 8.3 milliards de tonnes de plastique produit, dont la plupart sont des produits jetables et donc déchets qui finissent dans les mers et provoquent des dégâts majeurs sur la biodiversité.

![mer de plastique](images/merdeplastique.jpg)

#### Production 

Mais le problème n'est pas que le déchet, il intervient aussi à la source de la production du plastique car pour faire du plastique, il faut une certaine quantité de pétrole qui est une ressource non-renouvelable. Pour produire 1kg de plastique PET (celui qui compose en général les bouteilles). Pour la production le pétrole est utilisé de deux manières : matière première ainsi que pour le processus de production (machines, transports, ...).

#### Recyclage 

Le recyclage du plastique n'est pas une solution et reste problématique pour diverses raisons :

- Seulement 30% du plastique est collecté pour être reclyclé, donc le reste est enfoui ou incinéré
- le recyclage n'a pas un rendement de 100%
- certains objets sont recyclés en objets non-recyclables, donc cela revient au même 
- Les différences de plastiques rendent difficile le recyclage
- Le plastique n'est pas recyclé localement

#### Mer de plastique 

On sait qu'une grande partie des déchets plastiques se retrouvent dans nos mers et océans. Selon une étude de la [Fondation Ellen MacArthur](https://www.ellenmacarthurfoundation.org/assets/downloads/The-New-Plastics-Economy-Rethinking-the-Future-of-Plastics.pdf), on peut estimer :

- 8 millions de tonnes de "gros déchets" plastiques qui correspond aux sacs en plastique, vaisselle, bouteilles,etc.
- 0.8 à 2.5 millions de tonnes de microplastiques dits "primaires" qui se retrouvent notamment dans les microbilles contenues dans certains cosmétiques, les fibres de vêtements synthétiques qui se détachent quand on les lave...

Ce qui est problématique, c'est que le temps que prend le plastique pour se dégrader est extrèmement long, d'autant plus en mer. 
Voici un schéma publié par [SPF Santé Publique](https://www.health.belgium.be/fr/flyer-combien-de-temps-pour-se-degrader-en-mer), sur base de chiffres NOAA, qui reprend le temps de dégradation de différents déchets plastiques se retrouvant en mer. 

![degradation](images/degradationplastique.jpg)

#### Liens des mes recherches

- https://blog.design-market.fr/gaetano-pesce-le-designer-de-la-forme-libre/
- https://www.lemonde.fr/m-design-deco/article/2016/11/30/gaetano-pesce-designer-feministe_5040816_4497702.html
- https://www.ecoconso.be/fr/content/cest-quoi-le-probleme-avec-le-plastique
- https://www.nationalgeographic.fr/environnement/91-des-dechets-plastiques-ne-sont-pas-recycles
- https://www.ellenmacarthurfoundation.org/assets/downloads/The-New-Plastics-Economy-Rethinking-the-Future-of-Plastics.pdf
- https://www.health.belgium.be/fr/flyer-combien-de-temps-pour-se-degrader-en-mer


### PREMIER ESSAI : FONDRE DU PLASTIQUE 

Dans un premier temps j'ai du coup essayé de faire fondre du plastique, afin de voir le processus et le résultat que je pouvais obtenir en terme de solidité et surface. 

J'ai donc commencé par récolter différents types de plastique, notamment des bouteilles. Ensuite, sur [WikiHow](https://fr.wikihow.com/faire-fondre-du-plastique) j'ai trouvé différentes techniques de fonte. Celle qui me paraîssait la plus faisable était celle avec le four car j'ai un four à la maison. 

J'ai d'abord découpé les bouteilles en petits morceaux. 
Le problème lorsqu'on fait fondre du plastique, est qu'il faut simplement le faire fondre, sans le faire bruler. Ne sachant pas à quelle température le plastique des bouteilles fond, j'ai fait monter la température de manière progressive. Sur wikiHow, il était indiqué que la température était de 150°C, cependant cette température ne correspondait pas à celle des bouteilles en plastique. Le processus était donc assez long, et c'est en fait à partir de la température maximale de mon four que le platique a réellement commencé à fondre. 
A un moment donné, entre 150° et 250°C, je me suis dit que j'allais essayer directement sur le feu pour que ça aille plus vite : mauvaise idée ! Le plastique qui se trouvait dans le fond fondait, mais celui qui se trouvait au dessus ne fondait jamais, il faut une température uniforme pour que l'entièreté du plastique fonde. 

![fonte](images/processusfonteplastique.jpg)

Le résultat que j'ai obtenu est plutôt raté, je ne m'imaginais pas que la chose était aussi compliquée. Je suis quand même contente pour une première expérience, je me suis bien amusée à le faire. 

![brique plastique](images/briqueplastique.jpg)

Au final, le résultat le plus propre que j'ai obtenu, était le fond de plastique restant au fond de la cuvette. Quand j'ai terminé l'expérience, j'ai laissé la cuvette dehors dans le froid, et avec la contraction des matériaux qui est propre à chacun, le fond de plastique a eclaté et je me suis retrouvée avec une plaque de plastique ultra lisse. 

![fondfonte](images/fondfonte.jpg)

Suite à une discussion avec mon professeur, j'ai compris que pour faire une oeuvre politique, il était essentiel pour moi de choisir un matériaux en particulier et d'en connaître tout le bagage contextuel de la création de ce matériau pour pouvoir en faire une critique constructive. Car oui, parler de la consommation et de la pollution due au plastique est intéressant, mais c'est un sujet bien vaste, il me faut un matériaux avec un contexte bien précis sur lequel je peux rebondir. Je me suis alors reposé la question : 

**De quel matériau je veux parler en particulier et qui m'inspire le plus ?** 

J'ai alors songé à tous les déchets possibles et inimaginables, et l'idée du textile m'est venu. 


## L'INDUSTRIE TEXTILE, SURTOUT VESTIMENTAIRE 

### POURQUOI? 

Tout d'abord car j'ai toujours adorer coudre, je trafique depuis toujours mes vêtements et j'en ai confectionné quelques-uns moi-même. L'année passée, j'ai voulu arrêter l'architecture pour commencer des études en design textile, mais j'y ai renoncé pour des raisons personnelles. Ensuite, l'objet que j'ai choisi est une lampe qui prend la forme d'un drap posé sur une boule. C'est ce qui m'avait également plu dans l'objet. Enfin,l'industrie du textile est quelque chose qui me touche particulièrement. Depuis maintenant 5 ans, je n'achète plus de vêtements en magasin de grande marque type "fast-fashion", car je m'insurge contre le mode de production de ceux-ci (que j'expliquerais plus en détail après). Toute ma garde-robe ne provient que de magasins seconde-main,ce qui permet de donner une autre vie aux vêtements jetés. 
Si je parle de ce sujet, c'est également parce qu'on fait de plus en plus se rendre compte à la population des différents gaspillages tels que la nourriture, le plastique, mais les gens sont moins sensibilisés par rapport à l'industrie du textile qui est incroyablement néfaste pour la planète. 


### SURCONSOMMATION 

A l'heure actuelle, nous n'avons jamais autant consommé de vêtements. En 15 ans, la consommation de vêtement à augmenté de 60% notammant à cause de la "fast-fashion". Un européen achète en moyenne 20kg de vêtements chaque année, dont certains ne sont jamais portés et la plupart finissent à la poubelle après dix utilisations seulement. Au niveau mondial, c'est 56 millions de tonnes qui sont vendues chaque année. Le problème est double dans cette situation : 

1. L'industrie du textile est la **deuxième la plus polluante** du monde, elle dépasse même celle de l'aérien.
2. Le **gaspillage** des vêtements est collosal, des montagnes de vêtements sont brûlées chaque année. En moyenne, 80% des vêtements achetés finissent à la poubelle et seulement 12% de ceux-ci sont recylés ou ont une seconde vie. 


### ENVIRONNEMENTALEMENT

L'industrie du textile se place deuxième, juste après l'industrie pétrolière dans le classement mondial de pollution. Selon différentes études qui varient fortement, elle serait responsable de 3 à 10% des émissions de carbone. Mais bien souvent, le consommateur ne se rend pas vraiment compte que ces 3% sont déjà trop, avec des mécanismes qui sont bien encrés dans notre quotidien.

#### C'est quoi l'impact environnemental ? 

Bien souvent, quand on en parle, c'est généralement par rapport à l'**empreinte carbone** d'un produit, car aujourd'hui on considère que ce sont les **gaz à effet de serre** qui ont un principal impact sur les changements des cycles naturels planétaires. On mesure l'empreinte carbone en kg, qui correspond à une quantité de CO2 et de gaz à effet de serre rejeté par le système en question. Seulement, nombreux sont les **autres facteurs** qu'il faut étudier en terme de pollution pour avoir un bilan correct et complet : consommation d'eau douce, énergie venant des énergies fossiles, rejets toxiques dans les milieux aquatiques, impacts toxiques sur l'humain,... 

#### Pourquoi l'industrie est polluante ?

![déchets global textiles](images/dechetsglobalstextile.jpg)

Voici un schéma basé sur des chiffres étudiés en 2015. Il parle de lui même, et dénonce extractrion de ressources fossiles, enfouissement et incinération, non recyclage et lavages à répétitions pour l'entretien des vêtements.  A cela on peut ajouter l'immensité des déchets qui s'accumulent car l'industrie fonctionne d'une telle manière et c'est grâce à cela qu'elle survit : on produit, on achète, on jette. 
La production de vêtements ne se fait pratiquement qu'avec de nouveaux matériaux dont les trois quarts finissent finalement en décharge ou brûlés. En terme de chiffres, on a 73% des vêtements produits qui finissent en décharge ou incinérés, et 13% des matières utilisées qui seront recyclés.

#### L'impact environnemental dans sa globalité 

Il est important de calculer l'impact pendant l'intégralité de son cycle de vie, c'est à dire toutes les étapes qui interviennent dans la production des matières premières, les phases de fabrication, l'usage et la fin de vie du produit. C'est quelque chose de très complexe à calculer mais voici les principales choses à retenir: 

- **Matières premières** : il faut d'abord cultiver des fibres naturelles pour ensuite les récolter et les emballer. En ce qui concerne les fibres synthétiques, on calculera l'extraction du pétrole ainsi que la transformation de celui-ci pour arriver au fil. 
- **Fabrication** : il y a d'abord la confection du tissu (sous toutes ses formes) puis la fabrication du vêtement. Il faut bien prendre en compte le transport entre chaque étape ainsi que les déchets qui en ressortent. 
- **Distribution** : emballage et packaging des produits, transport, et ne pas oublier la consommation énergétique des lieux de vente. 
- **Usage** : lavge, séchage, repassage et les déchets que cela produit.
- **Fin de vie** : incinération ou recyclage (qui est moindre) et le transport qui y est associé. 

Voici un petit schéma de la fondation [Ellen Mac Athur](https://www.ellenmacarthurfoundation.org/assets/downloads/publications/A-New-Textiles-Economy_Full-Report_Updated_1-12-17.pdf)

![flux](images/schemafluxtextile.jpg)

#### La vie d'un t-shirt

Pietra Rivoli a un jour fait un périple dans le but de suivre toutes les étapes de production d'un simple t-shirt afin de mieux comprendre. Elle relate son aventure dans son livre "_Les Aventures d'un tee-shirt dans l'économie globalisée_",  édition Fayard, Paris, 2007. Un simple t-shirt qui est porté par tout le monde et qui se vend à plus ou moins 2 milliards de pièces par an dans le monde. 

Ce qu'on en retient : 

- L'empreinte carbone est de plus ou moins 5kg, c'est à dire 20x son poids propre. Cette donnée comprend les différentes étapes de production du produit jusqu'à sa mise en vente. Si on ajoute à cela l'empreinte de son usage et de sa fin de vie on peut facilement monter jusqu'au 10kg et plus. 
- Il a nécéssité 3750 L d'eau pour sa confection, notamment pour la culture du coton. 
- Il parcourt parfois plus de 40 000km. 

#### Distance parcourue

Il faut bien avoir en tête que chaque étape prend lieu dans un endroit différent. Voici un exemple relaté par Pietra :

- **Culture du coton** : cultivé à Lubbock (Texas)
- **Départ pour la Chine** : avant tout, le coton brut est transporté par voie terrestre au port de Long Beach (Californie) pour être ensuite expédié dans des conteneurs à Shangaï
- **Transformation de la matière première et fabrication du t-shirt** : dans l'exemple donné, la confection du fil ainsi que la réalisation du tissu et t-shirt se déroule à Shanghai, mais les principaux ateliers de confection se situent en Chine, Inde, Bangladesh et Tuquie, ce qui ajoute encore de la distance. 
- **Impression et mise en vente** : le t-shirt est ensuite renvoyé à Miami aux USA, où ils seront imprimés puis mis en vente. 
- **Seconde vie** : le t-shirt a de forte chance de terminer en Tanzanie, qui est le 4ème client des USA pour les vêtements d'occasion. 

#### Fin de vie ? 

Un documentaire intitulé "_La montage textile : Le fardeau caché de notre gaspillage vestimentaire_", filmé et édité par Fillipe Lopez, permet de mieux se rendre compte de l'ampleur des dégats, je conseille vivement de le regarder. Vous pouvez trouver la vidéo sur le site wecf France via ce [lien](https://wecf-france.org/court-metrage-documentaire-la-montagne-textile/).

![montagne textile](images/montagnetextileriviere.jpg)

Dans le reportage, l'exemple du Kenya est pointé du doigt. En Europe, nous jettons en moyenne **2 millions de tonnes** de vêtements chaque année, dont 70% sont envoyé au Kenya. Là bas, quand la vente de "_mitumba_", (c'est de cette manière que l'on nomme les vêtements récupérés), a commencé ils n'étaient au départ que quelques vendeurs, mais à partir des année 90, la pratique s'est beaucoup popularisée avec des arrivages de vêtements de plus en plus nombreux. Les arrivages viennent dans des conteneurs sous forme de gros ballots de vêtements qui sont investis par les vendeurs. Le problème, c'est que plus le temps passe, plus les vêtements sont de **moindre qualité** : en moyenne, 50% des vêtements contenus dans les ballots sont jetés directement. 

Là bas, les déchets ne sont pas traités de la même manière qu'ici: là où il y a de la place, on balance les ordures. Les rivières sont remplies des déchets textiles qui polluent et émanent des odeurs partout. Un homme qui s'occupe de nettoyer les zones affirme qu'apprès avoir finement ramassé toute la journée les déchets d'une zone, le lendemain, elle est de nouveau remplie de détritus. Les decheteries posent vraiment problème : les fumées dégagées causent des **problèmes respiratoires** graves à la population, atteignant surtout les enfants qui aiment jouer dans les déchèteries. Un hopital a été expressément ouvert pour traiter les maladies dûes à ce phénomène. 

En moyenne, on estime que 20 millions de kg de textiles sont déposés à la déchage de Nairobi chaque année. En 2001, la décheterie Pandora a été décrétée pleine, mais pour aujourd'hui, elle continue d'être exploitée. 


### DES GROSSES FIRMES COMME PRIMARK 

Primark propose des prix records pour la mode, "_la mode à petit prix_", avec une moyenne qui varie entre 4 et 6€ sur tout le magasin. 

#### Comment c'est possible ? 

- La marque n'utilise pas de publicité 
- leurs loyers sont négociés au rabais grâce à leur statut
- il n'y a pas de décoration dans les magasins
- leurs vêtements sont directement importés du Bangladesh 

Quelles sont alors leurs stratégies pour attirer les fournisseurs ? 

- ils font de grandes commandes qui leur permet de faire des économies d'echelle
- ils commandent à l'avance les produits les plus vendus 
- ils annulent très rarement leurs commandes et payent dans un délais très rapide

#### impact

On pourrait se dire que c'est bien d'un point de vue social, les prix bas permettent à tout le monde de suivre la mode à bas prix. Seulement tout ceci n'est que perversion, la marque base son profit sur l'**addiction** des consommateurs à acheter toujours plus et à très bas prix, ce qui pousse à la surconsommation. Ce modèle n'est **pas durable et viable** ni pour l'environnement, ni pour les travailleurs, ni pour le consommateur. D'autant plus que les conditions de productions sont très douteuses. 

#### Conditions de fabrication 

Le **Bangladesh** est à ce jour le 2ème fournisseur de l'Union Européenne, son industrie dépasse celle de la Chine et ne veut absolument pas décroitre. Mais quelle vie pour les ouvriers des usines ?

En effet pour une usine, il est impératif de maintenir des salaires extrèmement bas pour pâlir à la concurence, ce qui mène à des conditions de travail abérrantes. On ressasse aujourd'hui 4500 usines textiles au Bangladesh pour plus de 35 millions d'ouvriers dont la plupart sont des **femmes**. En moyenne leur salaire varie entre 80 et 100€ (beaucoup plus proche des 80) par mois pour des horaire de 10h par jour. Mais ce n'est pas tout : 

- salaire de misère
- répression syndicales 
- menaces d'expulsion 
- rapports de force 
- accidents à répétitions 
- expositions à des substances toxiques

![made in bangladesh](images/madeinbangladesh.jpg)

J'ai regardé le film "Made in Bangladesh" de Rubaiyat Hossain sorti en 2019. Ce film relate l'histoire d'une jeune femme travaillant à l'usine et qui décide de ne plus se laisser faire entraînant les autres avec elle. Mis à part le fil conducteur romancé, le film montre extrêment bien les conditions de travail dans lesquels ces jeunes femme vivent. Voici ce que j'en tire : 

Dans cette **société patriarcale**, l'emploi de femmes est fréquent car ils pensent qu'elles sont plus facile à contrôler, à manipuler. Les femmes là bas **ne connaissent pas leurs droits** et ne savent pas ce que les patrons ont le droit de faire. Elles sont bloquées dans une situation de surcontrôle, où les menaces d'expulsion flottent constamment au dessus de leur tête. Ce qu'elles ne peuvent biensur pas se permettre car elles ont besoin de cet argent pour vivre. Pourtant les patrons ne manquent pas d'abuser de leur pouvoir : ils les poussent à toujours travailler mieux et plus vite, ne les payent qu'un mois sur deux et payent encore moins leurs heures supplémentaires et si elles râlent : menaces et violences. 
Le travail se fait dans des bâtiments insalubres et non sécuritaires : édifices mal construits sur fondations branlantes avec trop d'étages, pas de sortie de secours, installations éléctriques défectueuses, fenêtres baricadées, piles de matériaux inflammables, peu ou pas de formation en santé, ni de sécurité des travailleurs et des dirigeants,etc.  Il y a beaucoup d'accidents au travail pour lesquels les ouvriers ne sont pas indemnisés. Depuis 2005, on relate plus ou moins 1500 morts dans les incendies et éffondrements d'usines (Rana Plaza par exemple).

Malgré différents mouvements et de nouvelles directives données par les postes plus haut placés, ordonnant des conditions meilleures, la réalité sur terrain est bien différente et les nouvelles législations ne sont pas appliquées. Le seul espoir pour cette situation repose sur les multinationnalistes qui donnent les ordres... mais le consommateur n'aurait-il pas un rôle primordial dans tout ça ? Bien sur que si. Même si la société nous pousse à la surconsommation, il faut que les gens se réveillent et il faut qu'on réinvente notre mode de consommation, car sans achat il n'y a pas de production. Il faut revaloriser l'achat et plutôt qu'acheter 3 items, en acheter un un peu plus cher, plus éco-responsable et durable, dont le prix vaudra alors la valeur réelle marchand du bien acquis. 


### LIENS DES RECHERCHES

- https://www.ellenmacarthurfoundation.org/assets/downloads/publications/A-New-Textiles-Economy_Full-Report_Updated_1-12-17.pdf
- http://www.slate.fr/story/192678/vetements-recyclage-industrie-textile-duree-vie
- https://www.ledauphine.com/france-monde/2019/09/26/gaspillage-vestimentaire-les-solutions-pour-limiter-notre-impact-ecologique
- https://www.rtbf.be/tendance/green/detail_pourquoi-les-vetements-sont-ils-si-compliques-a-recycler?id=10544778
- https://www.notre-planete.info/actualites/10-achat-vetements-mode-pollution
- https://www.ciso.qc.ca/wordpress/wp-content/uploads/Bangladesh_document_d_information.pdf


## PLACE A L'EXPERIMENTATION 

### MATERIEL

D'abord, j'ai dû me procurer des vêtements. J'ai donc contacer une société d'import- export qui trie les vêtements jetés dans les poubelles de Bruxelles : [Rakitex](https://www.europages.fr/RAKITEX-SPRL/BEL111122-000018444001.html). J'ai été mise en contact avec le gérant qui pouvait m'en donner gratuitement. Il m'a demandé quel type de vêtement je cherchais et j'ai choisi des t-shirts hommes en coton. Premièrement parce que les surfaces sont plus grandes, ensuite car c'est l'item qui est le plus vendu autour du monde. 
Pour le moment j'ai fait mes essais avec des vieux t-shirts que je n'utilisais plus chez moi, j'ai rendez-vous samedi pour récupérer le lot. 


### ESSAIS DE TISSAGE

La première intuition qui m'est venue a été de découper les t-shirts en lamelles afin de pouvoir les tresser/tisser. 

![découpe](images/decoupetshirts.jpg)

Ensuite, je connaissais le macramé mais je n' en avais jamais fait; je me suis donc amusée à tester les différents noeuds que le procédé propose. 

- J'ai d'abord essayé les noeuds classiques, tournants et plats.

![tournant et plat](images/noeudtournantetplat.jpg)

- J'ai après essayé de faire quelque chose de plus complexe en appliquant les noeuds que j'avais appris.

![tessage complexe](images/tressagescomplexes.jpg)

Je me suis ensuite lancée dans le tissage des lamelles. J'avais déjà une petite idée d'où je voulais aller : je pensais à faire une lampe où la lumière diffusée passerait à travers le maillage des bandes, ce qui ferait un jeu sur les contrates et textures du tissu. Je ne savais juste pas encore vraiment quelle forme ma lampe prendrait.

Directement, j'ai pu constater une différence par rapport à la découpe de mes bandes selon si j'avais découpé dans le sens de la maille ou perpendiculairement. 
Lorsqu'on coupe perpendiculairement et qu'on étire la bande, celle-ci reste plate à l'inverse de l'autre sens où la bande à tendance à se contracter sur elle-même. Les deux réactions sont intéressantes. 

![etirage lammelles](images/etiragelamelles.jpg)

J'ai donc commencé à tisser et voilà le résultat que j'ai obtenu en mixant les différentes couleurs et sens de la découpe.

![resultat tissage lampe](images/resultattissagelampe.jpg)

Je trouve le résultat plutôt intéressant, j'aime particulièrement bien l'effet des triangles que le tissus forme sur les côtés verticaux. Mais du coup, dans l'autre sens cela me plaît moins, car j'aurais voulu que la surface soit entièrement couverte sauf aux extrémités. Les couleurs ne me plaisent pas non plus, mais je n'avais que celles-ci à disposition. Pour plus tard, j'imaginerais quelque chose de beaucoup plus coloré, pour que les couleurs se mélangent encore plus au croisement de deux lamelles. 

C'est tout ce que j'ai imaginé pour le moment. 


### RETOUR DU PREJURY 

Quelques remarques ont été faites concernant la lampe tissée imaginée au dessus:

- un processus intéressant mais l'objet non abouti
-  le tissu : un tissage ? Peut-être pa 
- objet qui ne correspond pas aux attentes de la réflexion 
-  un questionnement sur la forme figée 


## MATIERE PREMIERE

J'ai récupéré un bon nombre de vêtements voués à être jetés. Dedans, il y avait toutes sortes de matières de tissus différentes.

![matiere première](images/matierepremierevetements.jpg)


## UNE AUTRE DIRECTION

Je me suis alors questionnée sur l'utilisation du tissu d'une autre manière, au lieu de simplement le découper et le tisser:

- déchirer ? 
- remplir un tissu d'autres tissus pour en faire une masse ?
- brûler ?

Ce qui m'intriguait le plus était d'essayer de brûler le tissu pour voir comment les différentes sortes de matières réagissaient. 


### TRIER 

J'ai dans un premier temps trié toute la masse de vêtements que j'ai récupéré, selon les différentes compositions du tissu:

- coton
- viscose
- polyester
- modal
- lin 
- elastane
- nylon 


### BRULER 

Ensuite j'ai brûlé les tissus, me rendant vite compte que les résidus de cendres ne m'intéressaient pas tant que ça. Une question m'est alors venue : 

_CHAUFFER SANS BRULER, CA DONNE QUOI ?_ 


### CHAUFFER

Je me suis donc mise à la tâche. Pour cela j'avais besoin de materiel : propulseur à air chaud, plaque métallique (pour ne pas brûler le sol et pour que la chaleur se diffuse mieux), et un ustensile pour bouger le tissu sans me brûler.


#### Réactions des différentes matières 

![reactions](images/reactionmatieres.jpg)

Resultat trop chouette: on a vraiment différents types de réactions, certains fondent, d'autres se contractent, d'autres encore s'éfilent. 

Globalement, on remarque que les matières qui contiennent du coton ne font que brûler, les matières les plus intéressantes sont celles composées de plastique ! Je vais donc bien choisir les vetements qui vont me servir. 

### Un motif ?

Quand j'ai vu que certains matériaux se rétractaient, je me suis demandé s'il serait possible de créer un motif régulier, pour le moment la réaction était assez aléatoire. J'ai donc essayé de placer différentes pièces métalliques sur le tissu et le résultat a été plutôt incroyable: la matière vient se recroqueviller autour des pièces. Je me suis donc amusée à essayer plein de motifs avec toutes sortes d'objets qui résistent à la chaleur.

![motif](images/motiftissu.jpg)

Vous pourrez voir le processus sur la vidéo finale.


### QUE FAIRE DE TOUT CA ?

#### Une lampe

Avec tous ces tests, je me demandais bien vers où je pouvais aller, mais ce qui me semblait le plus intéressant était une lampe, car la lumière met parfaitement en valeur les variations du tissu. 

![matiere a la lum](images/matierealalumiere.jpg)


#### Une forme

J'ai donc imaginé plusieurs prototypes, en voici quelques croquis... 

![croquis](images/croquislampetissusdroite.jpg)

Puis, je me suis posée et demandé ce que je voulais vraiment: je ne voulais pas un effet statique. L'option d'accroche au mur était à éviter et compliquait la tâche. 

Un objet suspendu ? Autour duquel on pourrait tourner ? Ca me semblait être la meilleure solution. Le rond m'a parut évident pour le côté organique et tournoyant. 

![croquis](images/croquislampetissuronde.jpg)

Il me fallait automatiquement deux pièces afin de pouvoir coincer le tissu entre chaque. 


#### Dimensions 

![croquisdim](images/dimensionslampetissu.jpg)

Le choix s'est opéré par rapport aux dimensions des LED qui font 3m de long pour une épaisseur de 10mm. 
Pour faire une double bande de LED le périmètre intérieur devait faire 1,5m. Le rayon devait donc être égale à 23,87cm. J'ai ajouté 3cm pour le périmètre extérieur. 
En ce qui concerne l'épaisseur du bois, le maximum en mdf est 18mm, ce qui allait parfaitement car je pouvais superposer les LED. 

Car une image vaut mille mots, voici une vidéo du processus (sound up).

[![](http://img.youtube.com/vi/jnIRJaBsDzE/0.jpg)](http://www.youtube.com/watch?v=jnIRJaBsDzE "")

[![](http://img.youtube.com/vi/ovfBkhdQaLM/0.jpg)](http://www.youtube.com/watch?v=ovfBkhdQaLM "")

![A4final](images/a4_final.pdf)

