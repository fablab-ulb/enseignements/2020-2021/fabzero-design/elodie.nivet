# PORTRAIT

 ![](images/avatar-photo.jpg)

Salut, je suis Elodie Nivet, élève en première master dans la Faculté d'architecture de la Cambre-Horta (ULB). Cette année j'ai choisi l'option **Architecture et Design** et c'est donc sur cette plateforme que vous pourrez suivre l'évolutoin de mon apprentissage et de mon projet au sein de l'atelier.

## MON PARCOURS

Née en Belgique à Braine-L'Alleud, j'ai beaucoup déménagé étant petite, vogant de maison en maison. J'ai vécu pendant 10 ans en France, jusqu'à l'âge de mes 16 ans où je sui retournée vivre en Belgique dans une ville campagne appelée Florenville (Gaume). Cette maison on l'a retapé de A à Z, en ne laissant que les 4 murs. 
A la fin de ma secondaire, je suis partie vivre en Angleterre à Milton Keynes où j'ai étudié pendant un an l'**Art** et la **Psychologie**. A mon retour, n'ayant pas un bagage particulièrement complet en art j'ai décidé de me lancer dans l'architecture qui aliait pour moi créativité, inventivité, sciences, intellectualité,... Des études assez complètes.

## UN PEU PLUS SUR MOI

Depuis que je suis gosse, j'aime bricoler, dessiner, faire de mes mains... A l'heure d'aujourd'hui je m'exprime sur différents supports artistiques : peinture, dessin, sculpture, poterie, photographie, ... De plus j'ai toujours beaucoup apprécié la couture, étant assez petite, j'ai dû retoucher mes habits afin de pouvoir porter ce que je veux. J'ai également créé moi même quelques vêtements partant de zéro. 

Ci-après deux exemples de photos. Je travaille souvent le N&B. Pour plus de photos je vous invite à visiter mon [instagram](https://www.instagram.com/elodi_ldow/?hl=fr).

![photos](images/photographies.jpg)

Poterie

![pots](images/sculpture.jpg)

Dessins

![dessins](images/dessins.jpg)

## CHOIX OBJET

![raglamp](images/raglamp2.jpg)

Lors du premier cours d'option, nous nous sommes donné rendez-vous au Design Museum Brussels afin de choisir un objet dans la collection qui nous interpelait, plaisait. Mon choix s'est tourné vers la Rag Lamp Table de Gaetano Pesce. J'ai choisi principalement cet objet car je trouvais qu'il sortait du lot, comparé aux autres qui sont, je trouve, très controlés. La première idée qui m'est venue en le voyant : "mouvement figé", ce qui m'a assez plu car c'est un sujet que j'aime travailler dans mes photographies. 
Vous pouvez retrouver plus d'information sur cet objet, mon choix et le développement de mes idées dans [final-project.md](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/elodie.nivet/-/blob/master/docs/final-project.md).
