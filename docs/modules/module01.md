# 1. GITLAB

Cette semaine, j'ai travaillé sur le GitLab et pour ce faire j'ai dû comprendre comment fonctionnait un terminal, ainsi que le codage dans git. 

## PROCESSUS

### Bash shell

Avant toute chose, comme je suis sur windows, j'ai dû installer **Bash** sur mon ordinateur. Pour ce faire, j'ai suivit la procédure décrite sur ce [lien](https://korben.info/installer-shell-bash-linux-windows-10.html). Simple procédure qui demande à changer quelques paramètres sur son Pc et ensuite le redémarer.

### Git

J'ai ensuite installé **Git**. Etant sur windows, je l'ai fait depuis un [site](https://git-scm.com/downloads) qui me permettait de télécharger le lanceur de l'installation. J'ai eu cependant un problème lors du premier téléchagement : Git GUI et Git CMD apparaissaient mais pas **Git Bash**. Je l'ai simplement désinstallé et téléchargé une seconde fois et tout était en ordre. Puis j'ai du le configurer avec la procédure décrite dans le fichier [Domumentation.md](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git).

![git config](../images/git_config.PNG)

### Clef SSH 

Maintenant que Git était installé, je devais générer une **clef SSH** afin de connecter le serveur à mon Pc local. J'ai eu un peu de mal car je n'avais pas trouvé le lien expliquant la procédure à suivre, j'ai alors recherché une [vidéo](https://www.youtube.com/watch?v=Vi-WqFKYpnw&ab_channel=MichelJung) explicative sur Youtube pour m'aider, avec laquelle j'ai réussi à comprendre exactement comment ça fonctionnait. Cependant dans la vidéo, la commande qu'ils indiquent de faire est celle-ci : _ssh-keygen_ . Cette commande va en fait téléchargé une clef **RSA** par défaut : je l'ai remarqué en voyant le fichier quand il était téléchargé. J'ai donc recommencé la procédure mais avec cette fois la commande ci-après:

![keyssh](../images/Keygen.PNG) 

Une fois la clef SSH téléchargée, je l'ai ouverte dans **Atom** afin de pouvoir la copier et je suis venue l'insérer dans mon profil Git sur le net. 
Petite vérification :

![verif](../images/welcome_to_git_lab.PNG)

### Fichier local

J'ai ensuite **cloné** mon fichier "elodie.nivet" sur mon local. C'est ici qu'on trouve le lien SSH : 

![lien SSH](../images/clone_SSH.PNG)

J'ai suivi la procédure donnée [ici](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-via-ssh), en copiant la clef SSH du fichier sur GitLab et en utilisant le code donné dans le tuto. 

A présent que c'est installé, il existe une procédure (ci=après) qui permet de mettre à jour les dernières modifications faites sur le pc.

![pulladdpush](../images/procedure_habituelle.PNG)

N'ayant pas trouvé cette information dans un premier temps, j'ai, de mémoire d'après ce que le formateur disait, essayé de "push" pour charger un test de modification opéré, tout en restant sur le terminal : j'ai essayé de plusieurs manières mais aucune n'a vraiment abouti. Ci-dessous les différents messages que j'ai eu.

J'ai d'abord eu ce message avant de recommencer la manipulation pour cloner le fichier, que je n'ai pas compris. J'ai donc recommencé l'opération de clonage.

![prob git push](../images/problem_git_push.PNG)

Ici on voit que le clonage a bel et bien fonctionné mais en faisait la deuxième manip il y a erreur. Je pense que c'est parce que je n'ai pas été dirrectement dans le fichier que je venais de cloner. 

![mauvais fichier](../images/prob_git_push.PNG)

Et finalement lorsque j'ai ouvert le Bash depuis le fichier "elodie.nivet" cloné, j'ai eu ce message, qui n'était pas celui attendu. 

![Mauvais message](../images/mauvais_setupstream.PNG)

### Solution au problème 

Ce qui causait le problème, c'est que j'ai travaillé sur les deux interfaces (sur internet et mon local), sans faire d'abord une mise à jour sur mon local avec la commande "git pull". Quel intérêt de d'abord faire les mises à jour avant de travailler dans une interface ? Lorsqu'on travaille à plusieur sur un même fichier, il est impératif de voir les modifications faites par ses collègues avant d'ajouter quoi que ce soit.

J'ai dond fait un test et changé un des fichiers sur Atom pour après envoyé l'information sur git internet, afin de voir comment git allait réagir.

![atom](../images/test_atom.PNG)

En suivant la procédure habituelle voici le message d'erreur que j'ai reçu :

![git add](../images/git_push_merge.PNG)

Ce message explique clairement que le problème vient du fait de ne pas avoir en premier lieu chargé les différents changements qui ont été fait dans le GitLab. J'ai donc fait la commande "git pull" et j'ai eu ce message :

![pull error](../images/git_pull_error.PNG)

Suite à ça, j'ai recherché sur internet le problème en récrivant le message d'erreur et j'ai trouvé une solution sur le site [Stackoverflow](https://stackoverflow.com/questions/19085807/please-enter-a-commit-message-to-explain-why-this-merge-is-necessary-especially). J'ai suivi la procédure suivante 

![merge prob](../images/merge_error.PNG)

Puis j'ai refait la série "git pull", "git add -A", ... et le problème était résolu.

![résolu](../images/CA_FONCTIONNE.PNG)
![test réu](../images/test_reussi.PNG)

Conclusion au problème : toujours faire le commande "git pull" avant de modifier quoi que ce soit dans les fichiers.

## VIDEOS

Lors de ma formation pour Fusion 360, j'ai du beaucoup expérimenté le logiciel. Pour moi, l'outil le plus facile à utiliser pour montrer ce que j'ai appris sur ce logiciel est la vidéo. J'ai donc du trouver un moyen de poster ces vidéos sur le GitLab. 

### Screen d'écran

Avant tout je devais trouver un logiciel qui me permette d'enregistrer tout ce qu'il se passait sur mon Pc. J'ai sur mon ordinateur déjà un programme qui me permet de faire ces vidéos, seulement le problème est que pour certains logiciels il ne montre pas tout : ici dans Fusion 360 toutes le fenêtres que j'ouvrais ne s'affichaient pas, ce qui est problématique pour la bonne compréhension du tutorial. 

J'ai donc recherche un peu sur google comment faire et j'ai trouvé qu'il y a un logiciel qui s'appelle "[Screencast](https://knowledge.autodesk.com/community/screencast)" qui sert à ça. Cependant, une fois que je l'avais téléchargé et utilisé, je me suis rendue compte qu'il ne premet pas de télécharger les vidéos faites, on peut seulement les poster en ligne sur une sorte de drive Autodesk.

J'ai alors recherché une autre solution et je suis tombée sur le logiciel [Screencast O Matic](https://screencast-o-matic.com/) qui est vraiment super simple à utiliser et qui est gratuit. Pour l'utiliser c'est simple : il suffit de télécharger le logiciel et un fois fait, lorsqu'on le lance sur notre pc, le logiciel nous renvoie à la page web ce-dessous où il faut cliquer sur "Launch Free Recorder". Apparaît du coup une fenêtre pour démarer les screens et choisir les différents modes qu'on veut, ainsi qu'un cadre en traits tillés qui est la zone de capture. 

![omatic](../images/screencastomatic.jpg)

### Mettre la vidéo sur GitLab

J'ai eu beaucoup de problèmes pour charger ma vidéo sur le git, car la Documentation.md n'était pas beaucoup détaillée. J'ai donc recherché des explications sur internet et je suis tombée sur ce [lien](https://www.freeconvert.com/video-compressor) dans lequel se trouve une vidéo et un texte explicatifs que j'ai suivi. 
J'ai d'abord créé un nouveau fichier sur mon local via Git Bash avec la commande _mkdir videos_, dans lequel je suis ensuite venue ajouter mon fichier mp4 dedans. Puis j'ai fait la routine : "git add, git commit, git push" pour envoyer les modifiations sur le GitLab.
Ensuite j'ai voulu incorporer ma vidéo avec les markdown qu'ils indiquait, mais ça n'a jamais fonctionné. J'ai donc demandé conseil à une amie qui m'a dit d'utiliser le même codage que pour les images. Et effectivement c'est comme ça que ça a fonctionné (encadré en bleu).

![codevideo](../images/codevideo.jpg)
