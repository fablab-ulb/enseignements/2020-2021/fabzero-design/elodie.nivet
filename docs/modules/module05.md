# 5. FORMATION SHAPER ORIGIN

Cette semaine on a eu la chance pour certains étudiants de pouvoir faire une formation Shaper. 

## C'EST QUOI SHAPER ORIGIN  

C'est une CNC (commande numérique par calculateur) portable. C'est une machine munie d'une fraiseuse qui permet, d'après un dessin numérique de faire des incrustations, menuiseries personnalisées, gravures, lettrages, travail du bois fin,... L'avantage de cette machine par rapport à la CNC Classique, c'est qu'on peut travailler sur une zon plus grande, la CNC est limitée à une surface de 1 sur 0,4m. 

Comme la lasersaure, la machine ne travaille qu'avec des fichiers 2D SVG.

![shapermachine](../images/shapermachine.jpg)

## PRECAUSIONS 

- Tenue vestimentaire appropriée : ni vêtements amples ni bijoux. Tenir cheveux, vêtements et gants loins des pièces mobiles. 
- Etre dans une position solide (pied et corps), afin d'avoir un meilleur contrôle sur l'outil electrique.
- Plan de travail stable 
- Bien fixer le matériau sur le plan de travail 
- TOUJOURS relever la fraise quand on passe d'une forme à une autre 
- TOUJOURS brancher un aspirateur pour la poussière au conduit fait pour ça. 

## PREPARATION DE LA DECOUPE

| Etapes | Illustrations |
| ------ | ------ |
| Il faut d'abord s'assurer que le matériau soit bien fixé au plan de travail. Pour se faire, on peut utiliser des sert-joints, mais le mieux c'est d'utiliser du ruban adhésif afin qu'après découpe, les pièces restent attachées au plan de travail. Avec le ruban adhésif, bien appuyer sur toute la surface que tout colle bien | ![adhésif](../images/adhesif.jpg) |
| Placer le shapertape sur le matériaux, permet à la machine de détecter la zone de découpe pour qu'on puisse par la suite placer le dessin où on le souhaite sur le matériaux. Pour que ça fonctionne bien, il faut qu'il y ai au minimum 2 bandes de shapertape avec minimum 4 dominos par bande collée. Attention, ça ne fonctionnera pas si le shapertape ne se trouve pas dans le champ de vision de la caméra. On peut biensur découper par dessus le shapertape, mais c'est un item qui coute assez cher, donc s'il est possible de le réutiliser, faites-le  | ![shapertape](../images/shapertape.jpg) |
| Veillez à bien brancher l'aspirateur au conduit d'expulsion prévu à cet effet | ![aspi](../images/shaperaspi.jpg) |
| S'assurer que le boitier de protection est bien en place, pour écviter de se prendre des éclats dans les yeux ou autre | ![protect](../images/shaperprotect.jpg) |
| **Il est possible de changer la fraise** pour se faire :  |
| 1. Débancher la prise qui relie fraise et machine | ![debranch](../images/shaperdebranch.jpg) |
| 2. Voici deux outils bien utiles pour retirer la fraise | ![outils fraise](../images/changefraise.jpg) |
| 3. Utiliser la clé torx pour détacher le silot et ensuite retirer le silot de la machine en tirant vers le haut, faire bien attention car la fraise est fragile   | ![silo](../images/shperretirertete.jpg) |
| 4. A l'aide de la clef à molette, dévisez l'écrou dans lequel se trouve la frais (dans le sens contraire des aiguille d'une montre) et retirer la fraise. Remettez en une autre du diamètre souhaité et visant (même sens). C'est un peu le même principe que pour changer les mêches d'une perceuse | ![fraise](../images/fraise.jpg) |


##  REGLAGES SUR LA MACHINE 

| Explications | Illustrations |
| ------ | ------ |
| **Scan**. Avant tout réglage, il faut d'abord scanner la zone de travail. Il est impératif de couvrir une zone la plus grande possible, quitte à dépasser les limites du matériau (outil encadré en rouge). Il suffit après de séléctionner son fichier depuis la clef USB et de déplacer la machine pour positionner le dessin | ![scanoutil](../images/shaperscan.jpg) |
| **Dessiner**. Il y a également la possibilité de dessiner directement via l'écran, la précision du dessin n'est pas très ouf mais c'est assez pratique pour relier certaines pièces ou traits de découpe par exemple | ![dessiner](../images/shaperdessiner.jpg) |
| On passe maintenant aux réglages du fraisage |
| **Profondeur**. Attention, pour ne pas abîmer la fraise, il faut que la profondeur soit au maximum égale au diamètre de la fraise | ![prof](../images/shaperprofondeur.jpg) |
| **Diamètre**. Il faut toujours bien faire attention quand on change de fraise à bien préciser le diamètre de la fraise, sans quoi le découpe se fera en fonction du diamètre indiqué | ![diam](../images/shaperdiametre.jpg) |
| **Z Touch**. Cette outil sert à, lorsqu'on change la fraise, requalibre la distance qu'il y a entre la faise et le matériau. Encore une fois il est important de le faire pour que la découpe se passe bien | ![ztouch](../images/ztouch.jpg) |
| **Vitesse**. La vitesse de rotation de la fraise va modifier la propreté de la découpe, il est préférable de faire des tests directement sur le matériaux afin d'otenir le résultat optimal | ![v](../images/shapervitesse.jpg) |
| **Guider**. Cet outil permet de définir la manière dont on veut que la découpe se fasse par rapport au dessin. Les différentes propositions : coupe intérieure, extérieure, sur la ligne, par évidage (les options parlent d'elles-mêmes)  | ![guide](../images/modedecoupe.jpg) |

## LA DECOUPE

Maintenant que tous les réglages sont fait, il suffit de s'y mettre. Quelques remarques : pour enclancher la rotation de la fraise, il faut appuyer sur le bouton on/off qui se trouve en plein milieu de la machine. Enuite pour commencer, il faut se placer sur la zonde de découpe, une fois qu'on y est il faut faire descendre la fraise en appuyant sur le bouton vert sur la poignée droite. Attention un fois que la découpe du dessin est terminée, il faut TOUJOURS remonter la fraise, avec cette fois ci le bouton rouge/orange qui se trouve sur la poignée gauche. 
Ne vous précipitez pas, tenez bien la machine en main, et amuser vous. 
