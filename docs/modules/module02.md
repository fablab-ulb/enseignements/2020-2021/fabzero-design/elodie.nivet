# 2. APPRENTISSAGE DE FUSION 360

Cette semain j'ai travaillé sur la modélisation 3D de mon objet afin de comprendre mieux le programme 3D, pour ensuite faire une impression de ma modélisatoin.

## Procédé

Ci-après les principales commandes utilisées :

![commandes](../images/commandes.jpg)

J'ai d'abord commencé par le haut de l'objet en créant une section d'un cercle. Pour ce faire j'ai d'abord esquissé un triangle rectangle dont l'hypoténuse forme un arc. Ensuite avec l'outil "**Révolution**", j'ai séléctionné la _surface_ ainsi que l'_axe_ sur lequel je voulais faire la rotation sur 360°.

![revolulu](../images/c.jpg)

On passe maintenant au corps sinueux, plus organique de la lampe. Ce [lien](https://www.youtube.com/watch?v=_rqwZewYoJQ&ab_channel=BenTeK-ConceptionetImpression3D&fbclid=IwAR0AliQ868Jem-2_iBuv5nerAD9KU-gPiZeTSL-5wARw7NMZXc4hpe8zR_U) renvoie à une vidéo qui m'a beaucoup aidé.

### Première tentative

L'idée est de recréer un _nouveau plan_ perpendiculaire à l'axe principal de l'objet, avec une distance choisie, grâce à l'outil "**Construire**". Un fois le plan défini, j'ai dessiné une esquisse de ce qu'il se passerait si on faisait une coupe horizontale dans la lampe, afin d'obtenir un drapé. Quand l'esquisse est terminée, j'ai utilisé l'outil "**Lissage**" pour joindre les deux surfaces : il suffit de séléctionner les deux surfaces concernées en maintenant _Shift_ enfoncé. Et on répète les opérations.

![plateau esquisse lissage](../images/plateau.jpg)

Le **problème** que j'ai rencontré avec cette manière de faire, c'est que le raccord entre les différents lissages n'offre pas une belle continuité : c'est plutôt un raccord hasardeux. On peut voici ci-dessous deux manières : à droite un raccord simplement connecté, qui créé une cassure dans le mouvement ; à gauche un raccord tangent qui donne un mouvement plus fluide mais créé des bosses et fosses non voulues dans ce cas-ci.

![raccords](../images/raccords.jpg)


### Seconde tentative

L'idée ici était d'utiliser la même technique avec les nouveau plateaux et esquisses, mais de cette fois-ci faire le lissage seulement à la fin. Le résultat obtenu est beaucoup mieux : les différents raccord se font de manière plus fluide est lisse. C'est ce premier modèle que j'ai lancé en impression 3D.

![sapin 3 etapes](../images/sapin.jpg)


Le problème des deux applications précédentes, c'est que le mouvement n'est pas du tout contrôlé, on peut estimer plus ou moins la forme que va prendre l'objet, mais les drapés restent assez aléatoires, approximatifs.


### Troisième tentative

Cette fois ci j'ai utilisé l'outil "sculpt" qui permet de gerer vraiment tous les mouvement fait par surfaces, arrêtes ou point.

Les commandes utilisées :

![com sculpt](../images/sculpter.jpg)

#### Modifier le formulaire 

Dans ce tuto j'explique comment utiliser l'outil de base de la modification d'un objet. Je suis moi partie d'une sphère pour faire un cône ce qui était le plus logique en vue de la forme de ma lampe. 

[![](http://img.youtube.com/vi/MyzRhqBZKgI/0.jpg)](http://www.youtube.com/watch?v=MyzRhqBZKgI "")

#### Faire un drapé et subdiviser 

Ici je vous montre comment utiliser de manière un peu plus poussée l'outil _modifier le formulaire_ en montrant l'exemple de mon drapé, ainsi que comment utiliser _subdiviser_, très utilise pour venir faire des modification plus précises, ponctuelles.

[![](http://img.youtube.com/vi/R3A34mhlHro/0.jpg)](http://www.youtube.com/watch?v=R3A34mhlHro "")


#### Outil tirer et épaissir

L'outil _tirer_ m'a vraiment été très utile. Comme je l'explique dans le module 04, mon impression n'a pas directement fonctionné car ma forme ne touchait pas le sol partout. Et donc c'est cet outil qui m'a permis de résoudre le problème d'impression. 

[![](http://img.youtube.com/vi/20TYuD5nvrA/0.jpg)](http://www.youtube.com/watch?v=20TYuD5nvrA "")

#### La surface T-Spline est auto-concourante

Lorsque je pensais avoir terminer de sculpter ma forme, j'ai voulu appuyer sur "Terminer" pour valider cette forme mais j'ai eu ce message d'erreur 

![message d'erreur](../images/prob_intersec.PNG)

Ici un tuto pour expliquer comment je suis arrivée à bout de ce problème

[![](http://img.youtube.com/vi/e88h4UbXMH4/0.jpg)](http://www.youtube.com/watch?v=e88h4UbXMH4 "")

#### Résultat final 

Et voilà le résultat que j'ai obtenu. Au final, cet exercice m'aura permis de métriser vraiment bien fusion sous pas mal de ses angles. Ca a déjà été bénéfique pour mon projet où je devais modéliser un objet, grâce à fusion j'ai pu le faire en l'espace de quelques minutes. 

![final](../images/finalfusion.jpg)



