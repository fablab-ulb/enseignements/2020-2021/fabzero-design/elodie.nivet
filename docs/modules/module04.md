# 4. DECOUPE LASER

Cette semaine nous avons eu une formation sur le laser cutter, et l'exercice donné était de créer une lampe, qui s'inspirait d'une caractéristique de notre objet. La découpe laser est un procédé qui permet de faire des découpes et gravures sur des matériaux, grâce à la focalisation d'un rayon laser qui permet d'élever une température sur un point bien précis. 

## LA FORMATION 

Il y a deux machines à laser à disposition : Le lasersaure et fullspectrum.

### Lasersaure 

C'est une machine open-source qui est la plus puissante des deux. L'avantage est qu'elle a une surface de découpe de 122x61cm, avec une hauteur de 12cm. La vitesse maximale de découpe est de 6000mm/min. En générale on monte jamais jusqu'à cette vitesse car le temps que la machine atteingne cette vitesse, la découpe sera déjà presque terminée. Le résultat de la découpe ne sera alors pas homogène. La puissance du laser peut est de 100W et la machine est dotée d'un laser de type tube CO2 infrarouge.


### Fullspectrum

Cette machine est beaucoup plus petite et on la préfère pour des épaisseurs assez faible en général. Sa surface de découpe est de 50x30cm avec une hauteur maximal de 6cm. Le laser qui est un tube CO2 infrarouge a une puissance de 40W.

### 5 Pécausions

- Bien connaitre le matérau utilisé
- Toujours ouvrir la vanne d'air comprimé
- Aussi l'extracteur de fumée
- Connaitre le bouton d'arrêt d'urgence (lasersaure) et boutons "pause" et "stop" (fullspectrum)
- Avoir un extincteur au CO2 à proximité

### Matériaux

| Recommandés | Déconseillés | Interdits |
| ------ | ------ | ------ |
| Bois contreplaqué (plywood/multiplex) | MDF, fumée épaisse/nocive à long terme | PVC, fumée acide/très nocive |
| Acrylique (PMMA/Plexiglass) | ABS, PS, fond facilement/fummée nocive | Cuivre, réfléchit totalement le laser |
| Papier, carton | PE, PET, PP, fond facilement | Téflon (PTFE), fumée acide/très nocive |
| Textiles | Composites à base de fibres, poussières très nocives | Vinyl, simili-cuir, peut contenir de la chlorine |
| - | Métaux, impossible à découper | Résine phénolique, époxy, fumée très nocive |

## CONCRETEMENT 

### Préparation 

- ![boutonallum](../images/arreturg.jpg) Pour allumer la machine, il suffit de tourner le bouton d'arrêt d'urgence dans le sens des flèches. Pour l'éteindre il suffit d'appuyer dessus. 

- ![home](../images/home.jpg) Ramener le laser à sa position d'origine.

- ![open](../images/open.jpg) Ce bouton sert à importer le fichier voulu, en SVG ou DXF, mais le logiciel préfère le SVG. Il y a souvent des problèmes lorsqu'on importe des dossier qui viennent d'Autocad par exemple, ou InDesign. Car le logiciel a tendance à grouper différents traits, ou change les couleurs des traits qui sont déterminants pour la découpe (on y reviendra plus tard). 

### Positionnement de la tête 

- Il faut tout d'abord positionner le matériaux dans la machine sur la grille d'abeille.

- ![moveori](../images/moveorigin.jpg) Avec les outils "move" et "jog" on peut déplacer la tête où on veut. Par exemple ça peut être pratique pour régler la distance focale, de ramener la tête à un point plus proche de nous pour avoir plus de facilité. 

- ![petit support](../images/remiseaniv.jpg) Régler la distance focale grâce au support. La distance entre la tête du laser et son point focal est de 15mm. C'est pourquoi lorsqu'on a un matériaux fin, comme ici dans notre cas on a découpé des planches de polypropilaine de 0,7mm, on place le laser à 15mm de cette planche afin qui le point de découpe soit le plus petit possible. Lorsqu'on utilise des matériaux plus épais, il faut prendre la moitié de l'épaisseur et la soustraire à la distance focale, afin que la point focal se retrouve au milieu de l'épaisseur du matériau. 

### Réglages

- ![moveori](../images/moveorigin.jpg) Avec le bouton "Offset" on peut déplacer le dessin si nécessaire.

- ![run](../images/run.jpg) Grâce au deux flèches, on peut Vérifier si le dessin reste bien sur le matériaux : Le laser va parcourir son trajet le plus à l'extérieur. 

- ![v/p](../images/vitessepuissance.jpg) 
On arrive au réglages de la vitesse et puissance de la découpe. Il faut tout d'abord choisir la couleur de trait que l'on veut à l'aide du bouton "+". Il est important de mettre dans les couleurs dans l'ordre chronologique qu'on veut : Par exemple il vaut mieux faire les gravures d'abord, pour eviter que si en découpant d'abord, le matériaux à bouger les gravures soient bien au bon endroit. Pour ma part j'ai d'abord mis les gravure, puis les découpes internes et j'ai ajouté une couleur qui découpe juste le coutour et sépart toutes les pièces seulement à la fin. 
Le "F" c'est pour la vitessse de découpe et le "%" pour la puissance.
On peut retrouver [ici](https://github.com/nortd/lasersaur/wiki/materials) quelques exemple de vitesse et puissance pour des matériaux donnés.

### Important

- ![reffroidisseur](../images/refroidisseureau.jpg) Allumer le refroidisseur à eau. 

- ![extracteur fumée](../images/extractfum.jpg) Allumer l'extracteur de fumée, voir deux si on a un matériau qui fait beaucoup de fumée.

- ![vanne](../images/vanne.jpg) Ouvrir la vanne d'air comprimé, en tournant le bouton pour d'il soit parallèle au tube.

### Démarer 

- ![statut](../images/status.jpg) Ce bouton est indicateur du bon déroulement de la découpe, pour que celle-ci fonctionne correctement il faut que ce bouton soit vert. Lorsqu'il est rouge ou orange, on peut cliquer dessus et alors on peut voir les étapes qui n'ont pas été faite, comme par exemple allumer les refroidisseur à eau ou autre. 

- ![run](../images/run.jpg) C'est avec le bouton "run" qu'on démarrer la découpe. A coté de ce bouton se trouve deux boutons "pause" et "arrêt", ceux-ci parlent d'eux même. 

- ![boutonallum](../images/arreturg.jpg) S'il y a un problème, appuyer sur le bouton d'arrêt d'urgence. 



## RECHRECHES

### Brainstorming

![brainstroming](../images/brainstorm.jpg)

Faire un brainstorming m'a permis de rassembler toutes mes impressions, ce que je vois dans mon objet. Un petit conseil vraiment c'est de demander à un de ses camarades pour en discuter : personnellement c'est en discutant avec quelqu'un que mes idées ont plus découler.  

![recherches](../images/recherches.jpg)

Du coup pour je suis partie sur l'idée du mouvement, qui sera ici décomposé.
J'ai donc pensé à une forme basique : parallépipède rectangle, qui serait alors découpé en plusieurs couches qui tourneraient toutes à chaque fois de quelques degrés pour recréer le mouvement de la torsion, comme si on avait séquencé le mouvement.

## CONCEPTION 

### Dessin 

![dessin auto](../images/dessinautocad.jpg)

Le concept de la découpe laser, est des découper du matériaux sous format 2D. Il faut donc dessiner l'objet sur un logiciel capable de faire de la 2D. Le logiciel de prédilection est Inkscape qui est [téléchargable](https://inkscape.org/fr/release/inkscape-0.92.4/), car le DriveboardApp (programme sur qui est relié à la machine et qui lance la découpe) préfère les fichiers SVG qui sortent de Inkscape. La DriveboardApp peut également lire les fichiers DXF qui sortent d'Autocad, mais il les lit avec difficulté et il y a souvent des problèmes avec les couleurs ou groupement.

On peut aussi importer des documents PDF ou Illustator dans Inkscape. Dans mon cas ici j'ai eu beaucoup de problèmes de conversion de documents : j'avais commencé par déssiner mon dessin sur Autocad, ensuite j'ai fait une mise en page que j'ai enregistrer sous PDF, puis quand je l'ai importé sur Inskape il y avait déjà un cadre un plus qui ne devait pas être là qui est apparut, et ensuite sur la DriveboardApp un deuxième est apparut. Je ne pouvais pas lancer l'impression car l'App considérait ces cadres comme des lignes de découpes qui interféraient avec mon dessin. 

J'ai donc du remodifier mon fichier sur Inkscape, et quand j'ai réussi à trouvé le cadre et à le supprimer, j'ai eu un autre problème : alors que le dessin était à la bonne échelle sur Inkscape, quand je l'ouvrait sur la DriveboardApp le dessin était beaucoup trop grand. On a essayé de trouver où était le problème avec Axel (formateur) mais rien ne fonctionnait. Normalement ce problème apparaît souvent parce qu'on est pas dans les bonnes unités des mesures, qu'on peut changer dans les _Propriétés du document_ : 

![unités](../images/pxtocm.jpg)
Par défaut l'unité va se mettre en pixels, ayant dessiné en mm, c'est l'unité que j'ai mise.  

![verification dimensions](../images/verifdimensions.jpg)
Ca semblait fonctionner, car même en vérifiant dans la barre du haut les dimensions indiquaient être bonnes. Pourtant dans le DriveboardApp le dessin n'était toujours pas à l'échelle. 

J'ai donc recommencé sauf que cette fois-ci j'ai enregistrer mon dessin en format DXF sans faire de mise en page et je l'ai directement importer dans Inkscape. J'ai alors corrigé les défauts sur Inksacep : Dégrouper les éléments en faisant un clic droit sur la selection et en cliquant sur "ungroup". Et j'ai changé les couleurs qui avaient été changées. Un fois fini je l'ai exporté en SVG, et là le document fonctionnait parfaitement. 

CONSEIL : travailler directement sur Inkscape, et sinon importer un fichier DXF est mieux que de passer par le PDF. 



### Pièces découpées

Voici les vitesses et puissances que j'ai utilisée pour la plaque de 0,7mm de polypropilaine : Le vert c'est pour la gravure, et rouge et noir pour la découpe. 
![v/p](../images/vitessepuissance.jpg)

### Résultat

Et voici les pièces que j'ai obtenu.
![pièces](../images/decoupe.jpg)

![lampe1](../images/lampe1.jpg)

Une précision sur l'exercice donné, est que nous devions réfléchir la lampe pour ne pas utiliser d'autres patériaux, c'est à dire pas de colle ni papier collant. Dans mon cas j'ai dessiné des entailles de l'épaisseur de la plaque afin que les carrés tiennent ensemble. Mais à force de recommencé le dessin, j'ai oublié les fentes qui permettraient aux carrés s'imbriquer entre eux. Comme on peut le voir au dessus, les carrés sont simplement posés les uns sur les autres. 

### Correction 

J'ai fait une deuxième fois la lampe mais en cette fois ci ajoutant les entailles que j'avais oublié de faire avant. Le résultat est beaucoup plus propre, régulier et solide surtout ! On peut voir l'imbrication des entailles dans la deuxième photo ci-dessous.

![lampe2](../images/lampe2.jpg)

