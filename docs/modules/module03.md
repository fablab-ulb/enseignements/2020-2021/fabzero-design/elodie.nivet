# 3. IMPRESSION 3D

Durant cette semaine, on nous a formé à utiliser les imprimantes 3D qui sont des modèles Original Prusa i3 MK3S. Ce sont des imprimantes open source, c'est à dire que ce sont des imprimantes qui ont elles même été imprimées par d'autres imprimantes 3D : production exponentielle. L'avantage de ce procésus c'est qu'on peut alors changer et réimprimer toutes les pièces cassées où manquantes.

Je vais donc dans ce module vous montrer ce que j'ai apris.

## La machine

Avant toute chose, il a fallut installer le logiciel permettant d'organiser l'impression de notre modélisation 3D. Pour ce faire, j'ai tapé "prusa i3 mk3s" dans google et je me suis rendue sur le site de [prusa](https://www.prusa3d.fr/) pour télécharger le software qui correspond au modèle des ateliers : **I3 MK3S**. Arrivée sur le site j'ai été dans "TELECHARGER LES PILOTES ET FIRMWARE" et ensuite j'ai cliqué sur download for windows (car oui j'ai windows) dans "DRIVERS & APPS 2.2.9.1".

## Modèle 3D

Il paraît logique que pour faire une impression 3D, il faut une modélisation... qu'on travaillera ici sur Fusion 360. Vous pouvez voir mes modèles et mon apprentissage dans le module 2. Un fois qu'on a fini ce modèle, il faut en tirer un fichier STL afin que le logiciel PrusaSlicer puisse le reconnaitre. Lancez alors ce logiciel, ouvrez votre fichier STL et votre modélisation apparaît alors sur le plateau. 

## PrusaSlicer

![manipulations](../images/diff_manip.jpg)

Lorsque qu'on a le modèle sur le logiciel, il est important de bien organiser l'impression de l'objet. Il existe différents outils (ci-dessus) qui permettent de positionner l'objet de manière à ce que l'impression se déroule le plus simplement possible pour avoir une surface de contact au plateau maximale et en même temps d'engendrer un minimum de supports et porte-à-faut. Par exemple pour mon objet, qui est de forme compliqué mais assez simple à concevoir pour pour l'impression, il serait illogique de le positioner sur le coté comme dans l'exemple "rotation". 

## Paramètres

Ensuite il faut paramétrer cette impression, c'est-à-dire gérer les épaisseurs, les bordures, les supports,... Pour ce faire je conseille d'abord de se mettre en mode expert afin d'accéder à un plus grand nombre de données.
![expert](../images/expert.jpg)

![para impress](../images/parametres_impression.PNG)

### Couches et périmètre

Il est important à savoir qu'on utilise du filament PLA avec les machines, qui peut atteindre une hauteur de 0.2mm, qui restera donc une valeur fixe. Il existe biensur d'autres types de filament comme par exemple celui fait de fibre de bois, cependant c'est le PLA qui est le plus rapide et le plus facile à utiliser, c'est pour ça qu'on le préfère. Egalement important : la buse peut aller jusqu'à une épaisseur de 0.4mm. 
Pour ma part ici, j'ai mis une épaisseur de 3 couches pour les parois verticale et horizontale du haut, car c'est en général ce qui est conseillé. Par contre pour la paroi horizontale du bas, càd celle accollée au plateau, je ne voulais pas que ma forme soi fermée alors j'ai mis 0. Cependant à mon premier essai je n'avais pas compris cette donnée alors j'avais également mis 3 couches, et c'est au moment de l'impression que je me suis rendue compte qu'il y avait erreur sur mes paramètres. J'ai donc annulé pour changer et recommencer.

![paraperi](../images/couches_et_peri_fant.PNG)


![dessous](../images/couchesous.jpg)  Ici la couche du bas que la machine commençait à imprimer.

### Remplissage

Lorsque l'on créé un modèle, on créé un certain volume qu'il faut remplir (si on le souhaite) afin de le rendre plus ou moins résistant à la pression. Plus on va augmenter la densité du motif choisi, plus la pièce sera résistante, mais attention : il ne sert à rien de mettre 100% de densité, les motifs sont faits pour être résistants et on gâcherait de la matière à fait ceci. A noter : le gyroïde est la forme qui est la plus solide et en général la densité idéale est comprise entre 10 et 15%, on peut monter mais seulement si c'est vraiment nécessaire. 
Dans mon cas, je voulais que ma forme soie vide, donc j'ai mis 0%.

![rempliss](../images/remplissage.PNG)

### Jupe et bordure

Quand on imprime un objet, si on veut que l'impression fonctionne correctement, il faut s'assurer que la pièce soit bien attachée sur le plateau. Plus la surface de contact est grande, mieux l'objet tiendra un place. Il faut d'autant plus faire attention quand l'objet que l'on créé est haut et alors mettre une bordure assez grande que pour le faire tenir (la bordure vient se faire autour de la zone de contact de l'objet).

![bord](../images/bordure.PNG)

### Supports 

Les supports sont des sorte de petits échafaudages que la machine va imprimer quand on a des éléments en porte-à-faux, qui dépassent, car oui la machine ne peut pas imprimer dans le vide. Un petite astuce pour limiter les supports et qu'ils ne soient pas surrabondants, c'est de cocher "Support sur le plateau uniquement".
Dans mon cas aucun support n'était nécessaires en vue de ma forme qui se construit sur elle même. 

![supports](../images/supports.PNG)

## Premier objet 

Après que les paramètres sont mis en place, on peut cliquer en haut à gauche sur le plateau et sur le cube strillé en dessous à gauche pour voir les différentes strates, supports, ... afin de vérifier dans un premier temps tsi tout risque de se passer bien à l'impression. Ensuite quand tout est ok, il suffit d'exporter le G code, de mettre le fichier sur une carte SD et du l'introduire dans le port de la machine. 

![exportgcode](../images/exportg.jpg)

Sur la machine, il faut bien faire attention à ce que le plateau soit propre, cad qu'il ne reste pas de résidu d'une précédente impression, il faut bien mettre la température en fonction du fil qu'on utilise. Dans mon cas avec le PLA c'est 215°.

On peut voir sur la première image la bordure qui se créé. Je n'ai pas eu de soucis particulier pour cette impression, le plus gros du travail était au niveau de la modélisation. 

![impression](../images/bordetevo.jpg)

Et voilà mon beau petit fantôme !

![final1](../images/finalpremier.jpg)

## Deuxième objet

### Tentative 1

Comme expliqué dans le module 3, mon deuxième objet est une version contrôlée du premier. La modélisation de cet objet a vraiment été un casse tête et sa conception à sûrement été la partie le plus dure de mon apprentissage. 
Lors de ma première tentative de modélisation, grâce à l'outil scultp, je pouvais donner une épaisseur à mon objet. Problème étant, vu la forme compliquée de mon objet, lorsque je lui ai donné une épaisseur, celle ci n'était pas uniforme : a certains endroits elles était apparemment trop fine. Ce qui fait que lorsque j'ai importé mon fichier dans PrusaSlicer, j'ai eu pleins de problèmes.
Lorsque j'ai mis tous mes paramètres en place, voici ce que le visualiseur me donnait 

![prob impression](../images/mauvaismodel.jpg)

Comme on peut le voir, il y a quelque chose qui cloche : déjà où est passé ma forme ? On peut voir les supports et la bordure, mais le corps de mon objet a disparut. On se demande aussi pourquoi il y a autant de supports, alors que ma formest sensée pouvoir se construire sur elle même... C'est en discutant avec Gwen et Hélène qu'on en a déduit qu'il y avait un problème d'épaisseur. Comme dans ma première impression je n'avait pas mis d'épaisseur et que tout avait fonctionné à merveille, je me suis dit que j'alais faire la même chose pour que ça fonctionne. Je suis donc revenue dans fusion pour faire des modifications sur mon fichier. 

### Tentative 2

Une fois mon fichier fini, j'ai l'ai importé dans PrusaSlicer et j'ai mis les réglages que je souhaitais. Le message ci-dessous est alors apparût.  

![premier message extrusion](../images/premiermessageextrus.jpg)

J'ai donc essayé par apprès de changer plusieurs paramètres dans Prusa pour essayer de comprendre un peu mieux d'où venait le problème. A chanque fois j'avais ce message d'erreur qui s'affichait et qui m'indiquait qu'il détectait des "flux négatifs", c'est à dire des trous. 

![probextru2](../images/PROBPRUSA.PNG)

- Sur la première image (ci-dessous), on peut voir qu'il y a un rond rouge tout au dessus de mon objet. Je pensais donc au début que le problème venait de là. J'ai donc essayé de changer plusieur fois ma modélisation fusion en aplatissant ou agrandissant le chapeau de l'objet. Mais sans jamais aucune réussite. 
- J'ai également essayé de mettre des supports, mais en vient
- Dans un essai j'ai essayé le remplissage pour voir ce que ça allait donner. Et au final c'était assez étrange car le remplissage fonctionnait, ça voulait donc dire que le logiciel détectait ma forme comme une forme fermée, et pourtant j'avais toujours le message. 
- A un moment quand j'ai rééssayé de visualiser l'impression sans les supports, je me suis rendue compte que la pièce flottait dans l'air, et donc ça m'a fait pensé que, pour des raisons esthétique, je n'avais pas mis tout mon objet sur un seul plan pour que l'effet du drappé soit plus fort. 

![essais](../images/rechercheprobprusa.jpg)

### Tentative 3 : réussite !  

![aplatissement](../images/aplat.jpg)

Je suis donc encore un fois retournée dans mon fichier fusion pour que la zone de contact au sol se fasse sur le même plan partout. ET CA A ENFIN FONCTIONNE !! 

![reglages](../images/reglagesob2prusa.jpg)
![final2](../images/objet2prusa.jpg)

### Résultat final 

Voici ici une série de photos ce que l'impression à donné, je suis très contente du résultat.

![evolulu](../images/evolutionbien.jpg)

Ici on peut voir les diférents détails de l'impression comme le support et la couche du dessus. La couche du dessus a un peu moins bien fonctionné parce que je n'ais mis que 2 épaisseur partout. 
![details](../images/detailsimpress.jpg)

Un garçon m'a montré de l'UCL m'a montré un outil sur Prusa qui est plutôt pratique pour ce genre de détails qui s'appelle _Hauteur de couche variable_. Ce que propose cet outille c'est de faire un lissage de l'ojet en ajoutant plus de couche à certaint endroits, mais attention cet outil ajoute du temps à l'impression. Sur la droite de l'écran on peut voir une ligne bleu qui représente en fait l'axe Y de l'objet, donc ça hauteur. En gliquant dessus on ajoute de l'épaisseur à la hauteur voulu, moi en l'occurance j'en ajouterais au sommet. 

![lissageimpress](../images/lissageimpress.jpg)

Et voiçi le résultat final où je me suis amusée à mettre de la lumière en dessus. C'était mon but d'avoir une certaine transparence, c'est pour ça que j'avais mis une épaisseur de 2 couches seulement. 

![final](../images/finalumineux.jpg)

